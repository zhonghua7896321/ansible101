#!/usr/bin/env python3

import argparse
import json

def main():
    inventory = {}
    if args.list:
        inventory = example_inventory()
    elif args.host:
        inventory = empty_inventory()
    else:
        inventory = empty_inventory()
    print(json.dumps(inventory))


def example_inventory():
    return {
        'group': {
            'hosts': ['bender', 'fry'],
            'vars': {
                'example_var1': 'proxyeast',
                'example_var2': 'proxywest',
                'ansible_ssh_pass': 'alta3',
                'ansible_python_interpreter': '/usr/bin/python3'
            }
        },
        '_meta': {
            'hostvars': {
                'bender': {
                    'ansible_user': 'bender',
                    'ansible_host': '10.10.2.3'
                },
                'fry': {
                    'ansible_user': 'fry',
                    'ansible_host': '10.10.2.4'
                }
            }
        }
    }

def empty_inventory():
    return {'_meta': {'hostvars': {}}}

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--list', action='store_true')
    parser.add_argument('--host', action='store')
    args = parser.parse_args()
    main()
